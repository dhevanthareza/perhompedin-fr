# Yuarsi

> My super Nuxt.js project

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## DEVELOPMENT FLOW

1. untuk buat module jalankan generator.js
2. isikan inputan generator sesuai contoh (upper lower case nya)
3. akan menghasilkan file api(/api/namaModule.js), index(pages/app/..nama-module../index.vue), create(components/nama-module/namaModuleCreate.vue)
4. buka /api/index.js import namaModule.js(lihat contih nya didalam file index.js nya aja)


## BUILD

1. Buka file .env atau buat
2. ubah environment sesuai build environment yang diinginkan
3. ada development,local,dan production konfigurasi tiap environment ada di config.js
4. lalu build dengan perintah yarn build:ready

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
