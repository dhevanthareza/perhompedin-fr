import BaseAPI from './base'

export default class Banner extends BaseAPI {
    datatable(page, limit, search = '') {
        return this.$axios.$get('/admin/banner/datatable', {
            params: {
                limit,
                page,
                search
            }
        })
    }

    get(id) {
        return this.$axios.$get(`/admin/banner/${id}`)
    }

    getAll() {
        return this.$axios.$get(`/admin/banner/`)
    }

    create(payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/banner`, data)
    }

    update(id, payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/banner/${id}`, data)
    }

    delete(id) {
        return this.$axios.delete(`/admin/banner/${id}`)
    }
}