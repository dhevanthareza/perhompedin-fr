import BaseAPI from './base'

export default class AboutPerhompedin extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/about-perhompedin/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/about-perhompedin/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/about-perhompedin/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/about-perhompedin`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/about-perhompedin/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/about-perhompedin/${id}`)
  }
}
