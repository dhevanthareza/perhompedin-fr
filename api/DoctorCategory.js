import BaseAPI from './base'

export default class DoctorCategory extends BaseAPI {
    datatable(page, limit, search = '') {
        return this.$axios.$get('/admin/doctor-category/datatable', {
            params: {
                limit,
                page,
                search
            }
        })
    }

    all() {
        return this.$axios.$get('/admin/doctor-category/all')
    }

    get(id) {
        return this.$axios.$get(`/admin/doctor-category/${id}`)
    }

    getAll() {
        return this.$axios.$get(`/admin/doctor-category/`)
    }

    create(payload) {
        return this.$axios.$post(`/admin/doctor-category`, payload)
    }

    update(id, payload) {
        return this.$axios.$post(`/admin/doctor-category/${id}`, payload)
    }

    delete(id) {
        return this.$axios.delete(`/admin/doctor-category/${id}`)
    }
}