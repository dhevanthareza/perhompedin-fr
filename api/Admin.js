import BaseAPI from './base'

export default class Admin extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/dataadmin/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/dataadmin/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/dataadmin/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/dataadmin`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/dataadmin/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/dataadmin/${id}`)
  }
}
