import BaseAPI from './base'

export default class ModuleExample extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/module-example/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/module-example/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/module-example/`)
  }

  create(payload) {
    return this.$axios.$post(`/module-example`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/module-example/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/module-example/${id}`)
  }
}
