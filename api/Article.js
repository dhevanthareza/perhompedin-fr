import BaseAPI from './base'

export default class Article extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/article/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/article/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/article/`)
  }

  create(payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/article`, data)
  }

  photo(payload) {
    return this.$axios.$post(`/admin/article/editor/photo`, payload)
  }

  update(id, payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/article/${id}`, data)
  }

  delete(id) {
    return this.$axios.delete(`/admin/article/${id}`)
  }
}
