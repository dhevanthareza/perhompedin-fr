import BaseAPI from './base'

export default class User extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/user/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/user/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/user/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/user`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/user/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/user/${id}`)
  }
}
