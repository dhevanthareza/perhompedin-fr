import BaseAPI from './base'

export default class Topic extends BaseAPI {
    datatable(page, limit, search = '') {
        return this.$axios.$get('/admin/topik/datatable', {
            params: {
                limit,
                page,
                search
            }
        })
    }

    all() {
        return this.$axios.$get('/admin/topik/all')
    }

    questionDatatable(page, limit, search = '', topicId) {
        return this.$axios.$get(`/admin/topik/${topicId}/question/datatable`, {
            params: {
                limit,
                page,
                search
            }
        })
    }

    get(id) {
        return this.$axios.$get(`/admin/topik/${id}`)
    }

    getAll() {
        return this.$axios.$get(`/admin/topik/`)
    }

    create(payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/topik`, data)
    }

    update(id, payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/topik/${id}`, data)
    }

    delete(id) {
        return this.$axios.delete(`/admin/topik/${id}`)
    }
}