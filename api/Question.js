import BaseAPI from './base'

export default class Question extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/soal/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/soal/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/soal/`)
  }

  create(data) {
    const payload = this.buildFormData(data)
    return this.$axios.$post(`/admin/soal`, payload)
  }

  update(id, payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/soal/${id}`, data)
  }

  delete(id) {
    return this.$axios.delete(`/admin/soal/${id}`)
  }
}
