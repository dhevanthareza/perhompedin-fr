import BaseAPI from './base'

export default class Doctor extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/doctor/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/doctor/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/doctor/`)
  }

  create(payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/doctor`, data)
  }

  update(id, payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/doctor/${id}`, data)
  }

  delete(id) {
    return this.$axios.delete(`/admin/doctor/${id}`)
  }
}
