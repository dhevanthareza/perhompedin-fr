import BaseAPI from './base'

export default class CriticAdvice extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/critic-advice/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/critic-advice/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/critic-advice/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/critic-advice`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/critic-advice/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/critic-advice/${id}`)
  }
}
