import BaseAPI from './base'

export default class Video extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/video/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/video/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/video/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/video`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/video/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/video/${id}`)
  }
}
