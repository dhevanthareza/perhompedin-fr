import BaseAPI from './base'

export default class Thread extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/thread/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/thread/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/thread/`)
  }

  create(payload) {
    return this.$axios.$post(`/admin/thread`, payload)
  }

  update(id, payload) {
    return this.$axios.$post(`/admin/thread/${id}`, payload)
  }

  delete(id) {
    return this.$axios.delete(`/admin/thread/${id}`)
  }
}
