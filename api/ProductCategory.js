import BaseAPI from './base'

export default class ProductCategory extends BaseAPI {
    datatable(page, limit, search = '') {
        return this.$axios.$get('/admin/product-category/datatable', {
            params: {
                limit,
                page,
                search
            }
        })
    }

    all() {
        return this.$axios.$get('/admin/product-category/all')
    }

    get(id) {
        return this.$axios.$get(`/admin/product-category/${id}`)
    }

    getAll() {
        return this.$axios.$get(`/admin/product-category/`)
    }

    create(payload) {
        return this.$axios.$post(`/admin/product-category`, payload)
    }

    update(id, payload) {
        return this.$axios.$post(`/admin/product-category/${id}`, payload)
    }

    delete(id) {
        return this.$axios.delete(`/admin/product-category/${id}`)
    }
}
