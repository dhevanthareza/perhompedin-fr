import Auth from './auth'
import Topic from './Topic'
import Agenda from './Agenda'
import Article from './Article'
import Video from './Video'
import User from './User'
import Admin from './Admin'
import Question from './Question'
import Thread from './Thread'
import Doctor from './Doctor'
import CriticAdvice from './CriticAdvice'
import Banner from './Banner'
import DoctorCategory from './DoctorCategory'
import AboutPerhompedin from './AboutPerhompedin'
import ProductCategory from './ProductCategory'
import Product from './Product'

export default class API {
    constructor($axios) {
        this.Auth = new Auth($axios)
        this.Topic = new Topic($axios)
        this.Agenda = new Agenda($axios)
        this.Article = new Article($axios)
        this.Video = new Video($axios)
        this.User = new User($axios)
        this.Admin = new Admin($axios)
        this.Question = new Question($axios)
        this.Thread = new Thread($axios)
        this.Doctor = new Doctor($axios)
        this.CriticAdvice = new CriticAdvice($axios)
        this.Banner = new Banner($axios)
        this.DoctorCategory = new DoctorCategory($axios)
        this.Product = new Product($axios)
        this.ProductCategory = new ProductCategory($axios)
        this.AboutPerhompedin = new AboutPerhompedin($axios)
    }
}
