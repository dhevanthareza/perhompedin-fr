import BaseAPI from './base'

export default class Agenda extends BaseAPI {
  datatable(page, limit, search = '') {
    return this.$axios.$get('/admin/agenda/datatable', {
      params: {
        limit,
        page,
        search
      }
    })
  }

  get(id) {
    return this.$axios.$get(`/admin/tes/${id}`)
  }

  getAll() {
    return this.$axios.$get(`/admin/agenda/`)
  }

  create(payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/agenda`, data)
  }

  update(id, payload) {
    const data = this.buildFormData(payload)
    return this.$axios.$post(`/admin/agenda/${id}`, data)
  }

  delete(id) {
    return this.$axios.delete(`/admin/agenda/${id}`)
  }
}
