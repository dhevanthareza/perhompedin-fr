import BaseAPI from './base'

export default class Product extends BaseAPI {
    datatable(page, limit, search = '') {
        return this.$axios.$get('/admin/product/datatable', {
            params: {
                limit,
                page,
                search
            }
        })
    }

    get(id) {
        return this.$axios.$get(`/admin/product/${id}`)
    }

    getAll() {
        return this.$axios.$get(`/admin/product/`)
    }

    create(payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/product`, data)
    }

    update(id, payload) {
        const data = this.buildFormData(payload)
        return this.$axios.$post(`/admin/product/${id}`, data)
    }

    delete(id) {
        return this.$axios.delete(`/admin/product/${id}`)
    }
}
