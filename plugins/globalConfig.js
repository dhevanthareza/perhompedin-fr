import config from '~/config'
const environment = process.env.NODE_ENV
export default (context, inject) => {
  const globalConfig = {
    gateway: config.gateway,
    agendaImagePath: `${config.gateway}/file/agenda`
  }
  inject('globalConfig', globalConfig)
}
