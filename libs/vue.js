export function vmodelMapper(
    keys = [],
    { prop = 'value', event = 'input', setter = {}, getter = {} } = {}
  ) {
    return keys.reduce((map, key) => {
      map[key] = {
        get() {
          const v = this[prop] && this[prop][key]
          return typeof getter[key] === 'function'
            ? getter[key].call(this, v, this[prop])
            : v
        },
        set(newValue) {
          if (typeof setter[key] === 'function') {
            setter[key].call(this, newValue, this[prop])
            return true
          }
          this.$emit(event, {
            ...this[prop],
            [key]: newValue
          })
        }
      }
      return map
    }, {})
  }
  
  export function computedValue() {
    return {
      computedValue: {
        get() {
          return this.value
        },
        set(v) {
          this.$emit('input', v)
        }
      }
    }
  }
  